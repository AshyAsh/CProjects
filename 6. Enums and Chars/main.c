#include <stdio.h>

int
main ()
{

  enum months
  {
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
  };

  enum months currentMonth = January;

  char myChar = 'A';

  printf("Current Month: %d\nMy Char value: %c\n", currentMonth, myChar);

  return 0;
}
