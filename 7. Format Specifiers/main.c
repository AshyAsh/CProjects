#include <stdio.h>

int
main ()
{
  int integer = 100;
  float floating = 331.79;
  double doubleVar = 8.44e+11;
  char character = 'W';
  _Bool boolean = 0;

  printf ("Interger: %i\n", integer);
  printf ("Interger: %d\n", integer);
  printf ("Floating: %f\n", floating);
  printf ("Double: %e\n", doubleVar);
  printf ("Double: %g\n", doubleVar);
  printf ("Character: %c\n", character);
  printf ("Boolean: %i\n", boolean);

  // Width specifiers determine the number/precision of which the values are
  // going to be printed, in the example below only 4.00 is going to be printed
  // as I set the width of x.2, that's to say, just the first 2 decimal numbers
  // (Note: It rounds the number)
  float x = 3.999323232;
  printf ("\nWidth specifier: %.2f\n", x);

  // Format specifiers
  // Type                    Examples                  Printf chars
  // char                    'a', '\n'                 %c
  // _Bool                   0, 1                      %i, %u
  // short int               __                        %hi, %hx, %ho
  // unsigned short int      __                        %hu, %hx, %ho
  // int                     12, -97, 0xFFE0           %i, %x, %o
  // unsigned int            12u, 100U, 0xFFu          %u, %x, %o
  // long int                12L, -2001, 0xffffL       %li, %lx, %lo
  // unsigned long int       12UL, 100ul, 0xffeeUL     %lu, %lx, %lo
  // long long int           0xe5e5e5e5LL, 50011       %lli, %llx, %llo
  // unsigned long long int  12ull, 0xffeeULL          %llu, %llx, %llo
  // float                   12.34f, 3.1e-5f           %f, %e, %g, %a
  // double                  12.32, 3.1e-5, 0x1.p3     %f, %e, %g, %a
  // long double             12.341, 3.1e-51           %Lf, %Le, %Lg

  return 0;
}
