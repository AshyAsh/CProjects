#include <stdio.h>

int
main ()
{
  // An int variable can only stores integer values: -1, 0, 1
  // For example: 100, -10000, 398
  int a = 10;

  // A float can store decimal numbers and be expressed in scientific notation
  // For example: .01, 0.017e4, 0xFEFD0A
  // NOTE: To define a float explicitly, it's necessary to put an "f" at the
  // end, for example: 12f
  float c = 23.333;

  // A double is like "float" but its value can be larger
  double d = 55.555555555555555e+11;

  // Store only 0 or 1 (false/true respectively)
  _Bool e = 1;

  // Short means to use the specified datatype (if any) with a shorter amount
  // of memory
  short int f = 2600;

  // Long means to use the specified datatype (if any)  with a longer amount of
  // memory
  long int g = 200000000000000000;

  // Unsigned means to use the specified datatype (if any)  without sign, that
  // is to say, if the maximum range of the specified datatype is -2, 2, when
  // using unsigned it will be become 0, 4 (without negatives)
  unsigned int h = 1000000000;
}
