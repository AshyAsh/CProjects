#include <stdio.h>

int
main ()
{
  char str[100];
  int i;
  double x;

  printf ("Enter a string, an int and a double: ");
  scanf ("%s %d %lf", str, &i, &x);

  printf ("You entered: %s %d %lf\n", str, i, x);
  return 0;
}
