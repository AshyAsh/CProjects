#include <stdio.h>

// argc: Number of arguments passed (argument count)
// argv: An array of character pointers - strings (argument vector)

int
main (int argc, char *argv[])
{
  int numberOfArguments = argc;
  char *argument1 = argv[0]; // argv[0] is the program name (i.e "./main")
  char *argument2 = argv[1]; // argv[1] is an argument (i.e "hello")

  printf ("Number of arguments: %d\nArgument one: %s\nArgument 2: %s\n",
          numberOfArguments, argument1, argument2);

  return 0;
}
