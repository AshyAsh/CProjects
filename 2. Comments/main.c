/*
 * Author: AshyAsh
 * Purpose: This program prints out my name to the screen
 * Date: 2020-12-27 15:31:15
 */

#include <stdio.h>

int
main ()
{
  // Displays "Hello World!<newline>My nme is ash"
  printf ("Hello world!\nMy name is Ash\n");
  return 0;
}
