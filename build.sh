for FOLDER in *; do
  if [ -d "$FOLDER" ]; then
    cd "$FOLDER"
    ./build.sh
    cd ..
  fi
done
