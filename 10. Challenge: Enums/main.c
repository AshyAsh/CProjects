#include <stdio.h>

int
main ()
{

  enum things
  {
    FSF,
    GNU,
    KERNEL,
    STALLMAN,
    HERMADES,
    RITCHIE
  };

  enum things thing1 = KERNEL;
  enum things thing2 = FSF;
  enum things thing3 = HERMADES;

  printf ("%d\n%d\n%d\n", thing1, thing2, thing3);

  return 0;
}
