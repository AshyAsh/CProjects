#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char *argv[])
{
  double width = atoi(argv[1]);
  double height = atoi(argv[2]);

  double perimeter = (height + width) * 2.0;
  double area = width * height;

  printf (
      "The width of the rectangle is %.2f\nits height is %.2f\nThe perimeter "
      "of the rectangle is %.2f\nand its area is %.2f\n",
      width, height, perimeter, area);
}
